package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by CCA_Student on 23/04/2018.
 */
public class Test01_Login extends BaseTest {

    @Test
    public void testLogin() {//Step 1 Confirm we are on the welcome page
        assertTrue(welcomePage.checkCorrectPage());

        // Step 2 Click on the Login link and the Login page loads}
        welcomePage.clickOnLogin();

        //Step 3 Confirm that we are now on the Login page
        assertTrue(loginPage.checkCorrectPage());

        //Step 4 Login with Valid credentials
        loginPage.login("testuser","testing");

        //Step 5 Check that we're now on the user page
        assertTrue(userPage.checkCorrectPage());
    }
}
